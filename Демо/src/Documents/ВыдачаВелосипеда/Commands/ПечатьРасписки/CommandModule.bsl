
&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	//{{_КОНСТРУКТОР_ПЕЧАТИ(ПечатьРасписки)
	ТабДок = Новый ТабличныйДокумент;
	ПечатьРасписки(ТабДок, ПараметрКоманды);

	ТабДок.ОтображатьСетку = Ложь;
	ТабДок.Защита = Ложь;
	ТабДок.ТолькоПросмотр = Ложь;
	ТабДок.ОтображатьЗаголовки = Ложь;
	ТабДок.Показать();
	//}}
КонецПроцедуры

&НаСервере
Процедура ПечатьРасписки(ТабДок, ПараметрКоманды)
	Документы.ВыдачаВелосипеда.ПечатьРасписки(ТабДок, ПараметрКоманды);
КонецПроцедуры
